import { Body, Controller, Get, Param, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group } from './group.entity';
import { GroupsService } from './groups.service';

@Controller('groups')
export class GroupsController {

    constructor(private GroupsService : GroupsService) {}

    @Get()
    async getAllGroups() : Promise<Group[]> {
        return this.GroupsService.getAllGroups();
    }

    @Get(':id')
    async getGroupById(@Param('id') id : string) : Promise<Group> {
        return this.GroupsService.getGroupById(id);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createGroup(@Body() createGroupDto : CreateGroupDto) {
    if(createGroupDto)
        {
         return this.GroupsService.createGroup(createGroupDto);
        }
    }

    @UsePipes(ValidationPipe)
    @Patch()
    async updateGroup(@Body() updateGroupDto: UpdateGroupDto){
        if(updateGroupDto){
            return this.GroupsService.updateGroupById(updateGroupDto);
        }
    }

}
