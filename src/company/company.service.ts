import { Injectable, NotFoundException, UsePipes, ValidationPipe } from '@nestjs/common';
import {v1 as uuid} from 'uuid';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyRepository } from './company.repository';
import { Company } from './company.entity';



@Injectable()
export class CompanyService {

    constructor(
        @InjectRepository(CompanyRepository)
        private companyRepository: CompanyRepository,
    ) {}
    
    async getAllCompany(): Promise<Company[]>{
        return this.companyRepository.find();
    }

    @UsePipes(ValidationPipe)
    async getCompanyById(id: number): Promise<Company> {
        const company : Company = await this.companyRepository.findOne(id,{relations: ["users","requests"]});
        if (!company){
            throw new NotFoundException();
        }
        return company;
    }

    async getAdminCompanyById(id: number): Promise<Company> {
        const company : Company = await this.companyRepository.createQueryBuilder("company")
        .where(`company.id = ${id}`)
        .leftJoinAndSelect("company.users", "user")
        .leftJoinAndSelect("user.team", "userteam")
        .leftJoinAndSelect("company.requests", "request")
        .leftJoinAndSelect("company.teams","team")
        .leftJoinAndSelect("company.saledRewards","myReward")
        .leftJoinAndSelect("myReward.user","myrewarduser") 
        .leftJoinAndSelect("myReward.reward","reward") 
        .getOne();
        if (!company){
            throw new NotFoundException();
        }
        return company;
    }

    async createCompany(createCompany: CreateCompanyDto): Promise<Company>{
        const company: Company = await this.companyRepository.createCompany(createCompany);
        return company;
    }

    async updateCompanyById(updateCompany: UpdateCompanyDto): Promise<Company> {
        const company : Company = await this.companyRepository.updateCompany(updateCompany);
        return company;
    }



}
