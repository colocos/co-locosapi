import { EntityRepository, Repository } from "typeorm";
import { Company } from "./company.entity";
import { CreateCompanyDto } from "./dto/create-company.dto";
import { UpdateCompanyDto } from "./dto/update-company.dto";

@EntityRepository(Company)
export class CompanyRepository extends Repository<Company> {
    async createCompany(createCompanyDto: CreateCompanyDto): Promise<Company> {
        const {name} = createCompanyDto;
        const company = new Company();
        company.name = name;

        await company.save();
        return company;
    }

    async updateCompany(updateCompanyDto: UpdateCompanyDto): Promise<Company> {
        const {id, name} = updateCompanyDto;
        const company = await this.findOne(id);

        company.name = name;
        await company.save();
        return company;
    }

    async deleteCompany(id: number){
        await Company.delete(id);
    }
}