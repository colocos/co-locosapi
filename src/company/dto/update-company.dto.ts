import { IsNotEmpty, IsString } from "class-validator"

export class UpdateCompanyDto {
    @IsNotEmpty()
    id: number
    @IsNotEmpty()
    @IsString()
    name: string
}