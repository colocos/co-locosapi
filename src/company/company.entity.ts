import { MyReward } from "src/myreward/entities/myreward.entity";
import { Request } from "src/request/request.entity";
import { Team } from "src/team/team.entity";
import { User } from "src/user/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Company extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @OneToMany(() => Request,request => request.company)
    @JoinColumn()
    requests: Request[];

    @OneToMany(() => User,user => user.company)
    @JoinColumn()
    users: User[]
    
    @OneToMany(() => Team, team => team.company, { eager:true })
    teams: Team[];

    @OneToMany(() => MyReward,myReward => myReward.company)
    saledRewards: MyReward[];
}