import { Body, Controller, Get, Param, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { CompanyService } from './company.service';
import { Company } from './company.model' ;
import { CreateCompanyDto } from './dto/create-company.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/user/user.entity';
import { GetUser } from 'src/user/get-user.decorator';
import { Roles } from 'src/user/auth/roles.decorator';
import { RoleEnum } from 'src/user/auth/roles.enum';
import { RolesGuard } from 'src/user/auth/roles.guard';

@Controller('company')
export class CompanyController {

    constructor(private companyService : CompanyService) {}

    @Get()
    async getAllCompany() : Promise<Company[]> {
        return this.companyService.getAllCompany();
    }

    @Get('/admin/:id')
    @UseGuards(AuthGuard("jwt"), RolesGuard)
    @Roles(RoleEnum.Admin)
    async getAdminCompany(@GetUser()user: User,@Param('id') id: number) : Promise<Company> {
        return this.companyService.getAdminCompanyById(id);
    }

    @Get('/mycompany/:id')
    @UseGuards(AuthGuard("jwt"))
    async getCompany(@GetUser()user: User,@Param('id') id: number) : Promise<Company> {

        return this.companyService.getCompanyById(id);
    }

    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard("jwt"), RolesGuard)
    @Roles(RoleEnum.Admin)
    @Post()
    async createCompany(@Body() createCompanyDto : CreateCompanyDto) : Promise<Company> {
        return this.companyService.createCompany(createCompanyDto);
    }



    


}
