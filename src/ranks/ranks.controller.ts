import { Body, Controller, Delete, Get, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateRankDto } from './dto/create-rank.dto';
import { UpdateRankDto } from './dto/update-rank.dto';
import { Rank } from './rank.entity';
import { RanksService } from './ranks.service';

@Controller('ranks')
export class RanksController {
    constructor(private ranksService: RanksService){}

    @Get()
    async getRanks(): Promise<Rank[]>{
        return this.ranksService.getRanks();
    }

    @Get("/:id")
    async getRankById(id: string): Promise<Rank>{
        return this.ranksService.getRankById(id);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createRank(@Body() createRankDto: CreateRankDto): Promise<Rank>{
        return this.ranksService.createRank(createRankDto);
    }

    @UsePipes(ValidationPipe)
    @Patch()
    async updateRank(@Body() updateRankDto: UpdateRankDto): Promise<Rank>{
        return this.ranksService.updateRank(updateRankDto);
    }

    @Delete()
    async deleteRank(id: string){
        return this.ranksService.deleteRank(id);
    }

}
