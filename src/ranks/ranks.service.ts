import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRankDto } from './dto/create-rank.dto';
import { UpdateRankDto } from './dto/update-rank.dto';
import { Rank } from './rank.entity';
import { RankRepository } from './rank.repository';

@Injectable()
export class RanksService {
    constructor(
        @InjectRepository(RankRepository)
        private rankRepository: RankRepository,
    ){}

    async getRanks(): Promise<Rank[]>{
        return this.rankRepository.find()
    }

    async getRankById(id: string): Promise<Rank>{
        const rank : Rank = await this.rankRepository.findOne(id);
        if(!rank){
            throw new NotFoundException()
        }
        return rank;
    }

    async createRank(createRankDto: CreateRankDto): Promise<Rank>{
        return this.rankRepository.createRank(createRankDto);
    }

    async updateRank(updateRankDto: UpdateRankDto): Promise<Rank>{
        return this.rankRepository.updateRank(updateRankDto);
    }

    async deleteRank(id: string){
        if(this.getRankById(id)){
            await this.rankRepository.deleteRank(id);
        }
        return
    }
}
