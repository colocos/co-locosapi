import { Optional } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class UpdateRankDto {
    @IsNotEmpty()
    id: string
    @IsNotEmpty()
    @IsString()
    name: string
    @IsString()
    img: string
    @IsString()
    description: string
    
}