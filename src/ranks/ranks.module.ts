import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RankRepository } from './rank.repository';
import { RanksController } from './ranks.controller';
import { RanksService } from './ranks.service';

@Module({
  imports: [TypeOrmModule.forFeature([RankRepository])],
  controllers: [RanksController],
  providers: [RanksService]
})
export class RanksModule {}
