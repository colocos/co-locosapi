import { EntityRepository, Repository } from "typeorm";
import { Rank } from "./rank.entity";
import { CreateRankDto } from "./dto/create-rank.dto";
import { UpdateRankDto } from "./dto/update-rank.dto";

@EntityRepository(Rank)
export class RankRepository extends Repository<Rank> {
    async createRank(createRankDto: CreateRankDto): Promise<Rank> {
        const {name, img, description} = createRankDto;
        const rank = new Rank();
        rank.name = name;
        rank.img = img;
        rank.description = description;

        await rank.save();
        return rank;
    }

    async updateRank(updateRankDto: UpdateRankDto): Promise<Rank> {
        const {id, name, img, description} = updateRankDto;
        const rank = await this.findOne(id);

        if(name){
            rank.name = name;
        }
        if(img){
            rank.img = img;
        }
        if(description){
            rank.description = description;
        }
        await rank.save();
        return rank;
    }

    async deleteRank(id: string){
        await Rank.delete(id);
    }
}