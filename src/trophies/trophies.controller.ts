import { Body, Controller, Delete, Get, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateTrophyDto } from './dto/create-trophy.dto';
import { UpdateTrophyDto } from './dto/update-trophy.dto';
import { TrophiesService } from './trophies.service';
import { Trophy } from './trophy.entity';

@Controller('trophies')
export class TrophiesController {
    constructor(private trophiesServices : TrophiesService){}

    @Get()
    async getTrophies(): Promise<Trophy[]>{
        return this.trophiesServices.getTrophies();
    }

    @Get("/:id")
    async getTrophyById(id: string): Promise<Trophy>{
        return this.trophiesServices.getTrophyById(id);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createTrophy(@Body() createTrophyDto : CreateTrophyDto){
        return this.trophiesServices.createTrophy(createTrophyDto);
    }

    @UsePipes(ValidationPipe)
    @Patch()
    async updateTrophy(@Body() updateTrophyDto : UpdateTrophyDto){
        return this.trophiesServices.updateTrophy(updateTrophyDto);
    }

    @Delete()
    async deleteTrophy(id: string){
        return this.trophiesServices.deleteTrophy(id);
    }
}
