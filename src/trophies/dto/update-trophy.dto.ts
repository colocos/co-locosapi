import { IsNotEmpty, IsString } from 'class-validator'

export class UpdateTrophyDto {
    @IsNotEmpty()
    id: string
    @IsNotEmpty()
    @IsString()
    name: string
    @IsString()
    description: string
    @IsString()
    img: string

    
}