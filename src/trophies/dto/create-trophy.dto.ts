import { IsNotEmpty, IsString } from 'class-validator'

export class CreateTrophyDto {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsString()
    description: string;

    @IsString()
    img: string;

}