import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTrophyDto } from './dto/create-trophy.dto';
import { UpdateTrophyDto } from './dto/update-trophy.dto';
import { Trophy } from './trophy.entity';
import { TrophyRepository } from './trophy.repository';

@Injectable()
export class TrophiesService {

    constructor(
        @InjectRepository(TrophyRepository)
        private trophyRepository : TrophyRepository
    ){}

    async getTrophies() : Promise<Trophy[]> {
        return this.trophyRepository.find();
    }

    async getTrophyById(id: string) : Promise<Trophy> {
        const trophy : Trophy = await this.trophyRepository.findOne(id);
        if(!trophy){
            throw new NotFoundException()
        }
        return trophy;
    }

    async createTrophy(createTrophyDto: CreateTrophyDto): Promise<Trophy> {
        return this.trophyRepository.createTrophy(createTrophyDto);
    }

    async updateTrophy(updateTrophyDto: UpdateTrophyDto): Promise<Trophy> {
        return this.trophyRepository.updateTrophy(updateTrophyDto);
    }

    async deleteTrophy(id: string){
        if(this.trophyRepository.findOne(id)){
            await this.trophyRepository.deleteTrophy(id);
        }
        return
    }
}
