import { EntityRepository, Repository } from "typeorm";
import { Trophy } from "./trophy.entity";
import { CreateTrophyDto } from "./dto/create-trophy.dto";
import { UpdateTrophyDto } from "./dto/update-trophy.dto";

@EntityRepository(Trophy)
export class TrophyRepository extends Repository<Trophy> {
    async createTrophy(createTrophyDto: CreateTrophyDto): Promise<Trophy> {
        const {name, description, img} = createTrophyDto;
        const trophy = new Trophy();
        trophy.name = name;
        trophy.description = description;
        trophy.img = img;

        await trophy.save();
        return trophy;
    }

    async updateTrophy(updateTrophyDto: UpdateTrophyDto): Promise<Trophy> {
        const {id, name, description, img} = updateTrophyDto;
        const trophy = await this.findOne(id);

        if(name){
            trophy.name = name;
        }
        if(description){
            trophy.description = description;
        }
        if(img){
            trophy.img = img;
        }

        await trophy.save();
        return trophy;
    }

    async deleteTrophy(id: string){
        await Trophy.delete(id);
    }
}