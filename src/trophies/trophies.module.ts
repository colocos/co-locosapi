import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TrophiesController } from './trophies.controller';
import { TrophiesService } from './trophies.service';
import { TrophyRepository } from './trophy.repository';

@Module({
  imports: [TypeOrmModule.forFeature([TrophyRepository])],
  controllers: [TrophiesController],
  providers: [TrophiesService]
})
export class TrophiesModule {}
