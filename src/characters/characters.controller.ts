import { Body, Controller, Get, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { Character } from './character.entity';
import { CharactersService } from './characters.service';
import { CreateCharacterDto } from './dto/create-character.dto';
import { UpdateCharacterDto } from './dto/update-character.dto';

@Controller('characters')
export class CharactersController {

    constructor(private charactersService: CharactersService){}
    
    @Get("/getAllCharacters")
    async getAllCharacters(): Promise<Character[]>{
        return this.charactersService.getAllCharacters();
    }


    @Get("/:id")
    async getCharacterById(id: string): Promise<Character>{
        return this.charactersService.getCharacterById(id);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createCharacter(@Body() createCharacterDto: CreateCharacterDto): Promise<Character>{
        return this.charactersService.createCharacter(createCharacterDto);
    }

    @UsePipes(ValidationPipe)
    @Patch()
    async updateCharacter(@Body() updateCharacterDto: UpdateCharacterDto): Promise<Character> {
        return this.charactersService.updateCharacterById(updateCharacterDto);
    }
}
