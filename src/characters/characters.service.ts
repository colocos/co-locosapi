import { Injectable, NotFoundException, UsePipes, ValidationPipe } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Character } from './character.entity';
import { CharacterRepository } from './character.repository';
import { CreateCharacterDto } from './dto/create-character.dto';
import { UpdateCharacterDto } from './dto/update-character.dto';

@Injectable()
export class CharactersService {
    constructor(
        @InjectRepository(CharacterRepository)
        private characterRepository: CharacterRepository,
    ) {}

    async getAllCharacters(): Promise<Character[]>{
        return this.characterRepository.find();
    }

    @UsePipes(ValidationPipe)
    async getCharacterById(id: string): Promise<Character> {
        const character : Character = await this.characterRepository.findOne(id);
        if (!character){
            throw new NotFoundException();
        }
        return character;
    }

    async createCharacter(createCharacter: CreateCharacterDto): Promise<Character>{
        const character: Character = await this.characterRepository.createCharacter(createCharacter);
        return character;
    }

    async updateCharacterById(updateCharacter: UpdateCharacterDto): Promise<Character> {
        const character : Character = await this.characterRepository.updateCharacter(updateCharacter);
        return character;
    }

    async deleteCharacter(id: string){
        if(this.getCharacterById(id)){
            await this.characterRepository.deleteCharacter(id);
        }
        return
    }
}
