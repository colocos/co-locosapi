import { EntityRepository, Repository } from "typeorm";
import { Character } from "./character.entity";
import { CreateCharacterDto } from "./dto/create-character.dto";
import { UpdateCharacterDto } from "./dto/update-character.dto";

@EntityRepository(Character)
export class CharacterRepository extends Repository<Character> {
    async createCharacter(createCharacterDto: CreateCharacterDto): Promise<Character> {
        const {points} = createCharacterDto;
        const character = new Character();
        character.points = points;

        await character.save();
        return character;
    }

    async updateCharacter(updateCharacterDto: UpdateCharacterDto): Promise<Character> {
        const {id, points} = updateCharacterDto;
        const character = await this.findOne(id);

        if(points){
            character.points = points;
        }
        await character.save();
        return character;
    }

    async deleteCharacter(id: string){
        await Character.delete(id);
    }
}