import { Optional } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class UpdateCharacterDto {
    @IsNotEmpty()
    id: string
    @IsNotEmpty()
    @IsString()
    points: string
}