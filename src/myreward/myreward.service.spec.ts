import { Test, TestingModule } from '@nestjs/testing';
import { MyrewardService } from './myreward.service';

describe('MyrewardService', () => {
  let service: MyrewardService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MyrewardService],
    }).compile();

    service = module.get<MyrewardService>(MyrewardService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
