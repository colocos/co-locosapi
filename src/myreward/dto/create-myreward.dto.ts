import { Company } from "src/company/company.entity";
import { Reward } from "src/rewards/reward.entity";
import { User } from "src/user/user.entity";

export class CreateMyrewardDto {
  reward: Reward;
  user: User;
  company: Company;
}
