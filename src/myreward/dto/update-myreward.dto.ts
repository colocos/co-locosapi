import { PartialType } from '@nestjs/mapped-types';
import { CreateMyrewardDto } from './create-myreward.dto';

export class UpdateMyrewardDto extends PartialType(CreateMyrewardDto) {
  status: number;
}
