import { Company } from "src/company/company.entity";
import { Reward } from "src/rewards/reward.entity";
import { Shop } from "src/shops/shop.entity";
import { User } from "src/user/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class MyReward extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    
    @ManyToOne(() => Reward, reward => reward.saledRewards, {eager: true})
    reward: Reward;
    
    @ManyToOne(() => User, user => user.buyedRewards)
    user: User;

    @Column()
    status: number;

    @ManyToOne(() => Company, company => company.saledRewards)
    company: Company;
}
