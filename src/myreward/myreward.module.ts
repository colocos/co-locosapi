import { Module } from '@nestjs/common';
import { MyRewardService } from './myreward.service';
import { MyrewardController } from './myreward.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MyRewardRepository } from './myreward.repository';

@Module({
  imports:[TypeOrmModule.forFeature([MyRewardRepository])],
  controllers: [MyrewardController],
  providers: [MyRewardService],
  exports: [MyRewardService]
})
export class MyrewardModule {}
