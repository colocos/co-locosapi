import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MyRewardService } from './myreward.service';
import { CreateMyrewardDto } from './dto/create-myreward.dto';
import { UpdateMyrewardDto } from './dto/update-myreward.dto';
import { MyReward } from './entities/myreward.entity';

@Controller('myreward')
export class MyrewardController {
  constructor(private readonly myrewardService: MyRewardService) {}

  @Post()
  create(@Body() createMyrewardDto: CreateMyrewardDto) {
    return this.myrewardService.create(createMyrewardDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMyrewardDto: UpdateMyrewardDto): Promise<MyReward> {
    return this.myrewardService.update(+id, updateMyrewardDto);
  }

}
