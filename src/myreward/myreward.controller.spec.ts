import { Test, TestingModule } from '@nestjs/testing';
import { MyrewardController } from './myreward.controller';
import { MyrewardService } from './myreward.service';

describe('MyrewardController', () => {
  let controller: MyrewardController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MyrewardController],
      providers: [MyrewardService],
    }).compile();

    controller = module.get<MyrewardController>(MyrewardController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
