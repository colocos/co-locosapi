import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMyrewardDto } from './dto/create-myreward.dto';
import { UpdateMyrewardDto } from './dto/update-myreward.dto';
import { MyReward } from './entities/myreward.entity';
import { MyRewardRepository } from './myreward.repository';

@Injectable()
export class MyRewardService {
  constructor(
    @InjectRepository(MyRewardRepository)
     private myRewardRepository: 
     MyRewardRepository
  ){}
  
  create(createMyrewardDto: CreateMyrewardDto) {
    return this.myRewardRepository.createMyReward(createMyrewardDto);
  }

  findAll() {
    return `This action returns all myreward`;
  }

  findOne(id: number) {
    return `This action returns a #${id} myreward`;
  }

  async update(id: number, updateMyrewardDto: UpdateMyrewardDto): Promise<MyReward> {
    const editMyReward = await this.myRewardRepository.findOne(id);
    const updatedReward = await this.myRewardRepository.updateMyReward(updateMyrewardDto, editMyReward);
    const newReward : MyReward = await this.myRewardRepository.createQueryBuilder("myreward")
        .where(`myreward.id = ${updatedReward.id}`)
        .leftJoinAndSelect("myreward.user","user") 
        .leftJoinAndSelect("myreward.reward","reward") 
        .getOne()
    return newReward;
  }

  remove(id: number) {
    return `This action removes a #${id} myreward`;
  }
}
