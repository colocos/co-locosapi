// import { Reward } from "src/rewards/reward.entity";
import { Company } from "src/company/company.entity";
import { Reward } from "src/rewards/reward.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateMyrewardDto } from "./dto/create-myreward.dto";
import { UpdateMyrewardDto } from "./dto/update-myreward.dto";
import { MyReward } from "./entities/myreward.entity";


@EntityRepository(MyReward)
export class MyRewardRepository extends Repository<MyReward> {
    async createMyReward(createMyRewardDto: CreateMyrewardDto):Promise<MyReward>{
      const { reward,company,user } = createMyRewardDto;
      let newMyReward = new MyReward();

      newMyReward.reward = reward;
      newMyReward.company = company;
      newMyReward.user = user;
      newMyReward.status = 0;

      try{
        return await newMyReward.save();
      }
      catch (error){
        throw new Error(error)
      }
    }

    async updateMyReward(updateMyRewardDto: UpdateMyrewardDto, editMyReward: MyReward): Promise<MyReward> {
      const { status } = updateMyRewardDto;
      editMyReward.status = status;

      try{
        return await editMyReward.save();
      }
      catch (error){
        throw new Error(error)
      }
    }
}
