import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormConfig } from './config/typeorm.config';
import { Connection } from 'typeorm';
import { CompanyModule } from './company/company.module';
import { UserModule } from './user/user.module';
import { GroupsModule } from './groups/groups.module';
import { RequestModule } from './request/request.module';
import { MediaModule } from './media/media.module';
import { MulterModule } from '@nestjs/platform-express'; 
import { TeamModule } from './team/team.module';
import { CharactersModule } from './characters/characters.module';
import { RanksModule } from './ranks/ranks.module';
import { TrophiesModule } from './trophies/trophies.module';
import { ShopsModule } from './shops/shops.module';
import { RewardsModule } from './rewards/rewards.module';
import { MyrewardModule } from './myreward/myreward.module';


@Module({
  imports: [TypeOrmModule.forRoot(typeormConfig),CompanyModule, UserModule, GroupsModule, RequestModule, MediaModule,CharactersModule, RanksModule, TrophiesModule, ShopsModule, RewardsModule, MulterModule.register({
    dest: './media/files'
  }), TeamModule, MyrewardModule],
  controllers: [],
  providers: [],
})
export class AppModule {

  constructor(private connection: Connection) {}
}
