import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { Reward } from 'src/rewards/reward.entity';
import { CreateShopDto } from './dto/create-shop.dto';
import { UpdateShopDto } from './dto/update-shop.dto';
import { Shop } from './shop.entity';
import { ShopsService } from './shops.service';

@Controller('shops')
export class ShopsController {

    constructor(private shopsService: ShopsService){}

    @Get()
    async getShops(): Promise<Shop[]>{
        return this.shopsService.getShops();
    }

    @Get("/:id")
    async getShop(@Param("id", ParseIntPipe)id: number): Promise<Shop>{
        return this.shopsService.getShopByCompanyId(id);
    }

    @UsePipes(ValidationPipe)
    @Post()
    async createShop(@Body() createShopDto: CreateShopDto): Promise<Shop>{
        return this.shopsService.createShop(createShopDto)
    }

    @UsePipes(ValidationPipe)
    @Patch()
    async updateShop(@Body() updateShopDto: UpdateShopDto, rewards: Reward[]): Promise<Shop>{
        return this.shopsService.updateShop(updateShopDto, rewards);
    }
    
}
