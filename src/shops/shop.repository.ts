// import { Reward } from "src/rewards/reward.entity";
import { Company } from "src/company/company.entity";
import { Reward } from "src/rewards/reward.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateShopDto } from "./dto/create-shop.dto";
import { UpdateShopDto } from "./dto/update-shop.dto";
import { Shop } from "./shop.entity";

@EntityRepository(Shop)
export class ShopRepository extends Repository<Shop> {
    async createShop(createShopDto: CreateShopDto, company_related: Company):Promise<Shop>{
        const shop = new Shop();

        shop.rewards = [];
        shop.company = company_related;

        await shop.save();
        return shop;
    }

    async updateShop(updateShopDto: UpdateShopDto, rewards: Reward[], company_related: Company): Promise<Shop> {
        const {id, name, points} = updateShopDto;
        const shop = await this.findOne(id);

        if(rewards){
            shop.rewards = rewards;
        }
        if(company_related){
            shop.company = company_related;
        }

        await shop.save();
        return shop;
    }
}
