import { Company } from "src/company/company.entity";
import { Reward } from "src/rewards/reward.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Shop extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    
/*     @Column()
    name: string; */

    @OneToMany(() => Reward, reward => reward.shop_id)
    rewards: Reward[];

    @OneToOne(() => Company)
    @JoinColumn()
    company: Company;

    /* @Column()
    points: number; */

}