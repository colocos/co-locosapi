import { IsNotEmpty, IsNumber, IsString } from "class-validator"
import { Reward } from "src/rewards/reward.entity"

export class UpdateShopDto {
    @IsNotEmpty()
    id: number
    @IsNotEmpty()
    @IsString()
    name: string
    @IsNumber()
    company_id: number
    @IsNumber()
    points: number
}