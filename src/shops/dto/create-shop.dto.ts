import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Reward } from "src/rewards/reward.entity";
import { OneToMany } from "typeorm";
import { Shop } from "../shop.entity";

export class CreateShopDto {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNumber()
    company_id: number;

    @IsNumber()
    points: number;


}