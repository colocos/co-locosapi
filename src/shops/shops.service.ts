import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyService } from 'src/company/company.service';
import { Reward } from 'src/rewards/reward.entity';
import { CreateShopDto } from './dto/create-shop.dto';
import { UpdateShopDto } from './dto/update-shop.dto';
import { Shop } from './shop.entity';
import { ShopRepository } from './shop.repository';

@Injectable()
export class ShopsService {
    constructor(
        @InjectRepository(ShopRepository)
        private shopRepository: ShopRepository,
        private companyService: CompanyService
    ){}

    async getShops():Promise<Shop[]>{
        return this.shopRepository.find()
    }

    async getShopById(id: number): Promise<Shop>{
        const shop : Shop = await this.shopRepository.findOne(id);
        if(!shop){
            throw new NotFoundException()
        }
        return shop;
    }

    async getShopByCompanyId(id: number): Promise<Shop>{
        let shop : Shop = await this.shopRepository.createQueryBuilder("shop")
        .where(`shop.companyId = ${id}`)
        .leftJoinAndSelect("shop.rewards", "reward")
        .orderBy("name")
        .getOne();


        let newRewards = shop.rewards.filter(r => r.quantity > 0);
        shop.rewards = newRewards;
        if(!shop){
            throw new NotFoundException()
        }
        return shop;
    }

    async createShop(createShopDto: CreateShopDto): Promise<Shop>{
        const company_related = await this.companyService.getCompanyById(createShopDto.company_id);
        return this.shopRepository.createShop(createShopDto, company_related)
    }

    async updateShop(updateShopDto: UpdateShopDto, rewards: Reward[]): Promise<Shop>{
        const company_related = await this.companyService.getCompanyById(updateShopDto.company_id);
        return this.shopRepository.updateShop(updateShopDto, rewards, company_related);
    }

}
