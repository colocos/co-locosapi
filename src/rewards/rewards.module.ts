import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MyrewardModule } from 'src/myreward/myreward.module';
import { ShopsModule } from 'src/shops/shops.module';
import { UserModule } from 'src/user/user.module';
import { RewardRepository } from './reward.repository';
import { RewardsController } from './rewards.controller';
import { RewardsService } from './rewards.service';

@Module({
  imports: [ShopsModule,UserModule,MyrewardModule,TypeOrmModule.forFeature([RewardRepository])],
  controllers: [RewardsController],
  providers: [RewardsService]
})
export class RewardsModule {}
