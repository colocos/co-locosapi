import { MyReward } from "src/myreward/entities/myreward.entity";
import { Shop } from "src/shops/shop.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Reward extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    name: string;

    @Column()
    description: string;
    
    @ManyToOne(() => Shop, shop => shop.rewards)
    @JoinColumn({name: "shop_id"})
    shop_id: Shop;
    
    @Column()
    img: string;

    @Column()
    quantity: number;

    @Column()
    points: number;

    @OneToMany(() => MyReward, myReward => myReward.reward)
    saledRewards: MyReward[];
}