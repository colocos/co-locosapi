import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMyrewardDto } from 'src/myreward/dto/create-myreward.dto';
import { MyRewardService } from 'src/myreward/myreward.service';
import { Shop } from 'src/shops/shop.entity';
import { ShopsService } from 'src/shops/shops.service';
import { User } from 'src/user/user.entity';
import { UsersService } from 'src/user/user.service';
import { CreateRewardDto } from './dto/create-reward.dto';
import { UpdateRewardDto } from './dto/update-reward.dto';
import { Reward } from './reward.entity';
import { RewardRepository } from './reward.repository';

@Injectable()
export class RewardsService {
    constructor(
        @InjectRepository(RewardRepository)
        private rewardRepository: RewardRepository,
        private shopsService: ShopsService,
        private myRewardService: MyRewardService,
        private userService: UsersService
    ){}

    async getRewards():Promise<Reward[]>{
        return this.rewardRepository.find({relations: ["shop_id"]});
    }

    async getRewardsByShop(shop: Shop):Promise<Reward[]>{
      let rewardShop = await this.rewardRepository.find({where: {shop_id: shop}});
      return rewardShop;
    }

    async getRewardById(id: number): Promise<Reward>{
        const reward : Reward = await this.rewardRepository.findOne(id);
        if(!reward){
            throw new NotFoundException()
        }
        return reward;
    }

    async createReward(createRewardDto: CreateRewardDto,filename: string): Promise<Reward>{
        const shop_related = await this.shopsService.getShopById(createRewardDto.shop_id);
        return this.rewardRepository.createReward(createRewardDto, shop_related,filename);
    } 
    
    async updateReward(updateRewardDto: UpdateRewardDto): Promise<Reward>{
        return this.rewardRepository.updateReward(updateRewardDto)
    }  

    async buyReward(user: User,id: string): Promise<Reward> {
      const reward = await this.rewardRepository.findOne(id, {relations: ["shop_id"]});
      if(reward.quantity > 0 && (user.points - reward.points) > 0){
        let newMyReward = new CreateMyrewardDto();
        newMyReward.user = user;
        newMyReward.company = user.company;
        newMyReward.reward = reward;
        this.myRewardService.create(newMyReward);
        this.userService.updateUserPoints(user,reward.points);
        return this.rewardRepository.buyReward(reward);
      }
      else{
        throw new Error("Not enough quantity");
      }
    }

    async deleteReward(id: number) {
      await this.rewardRepository.delete(id);
    }
}
