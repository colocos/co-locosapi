import { Shop } from "src/shops/shop.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateRewardDto } from "./dto/create-reward.dto";
import { UpdateRewardDto } from "./dto/update-reward.dto";
import { Reward } from "./reward.entity";

@EntityRepository(Reward)
export class RewardRepository extends Repository<Reward> {
    async createReward(createRewardDto: CreateRewardDto, shop_related: Shop,filename: string):Promise<Reward>{
        const { name,quantity,description,points } = createRewardDto;
        const reward = new Reward();
        reward.name = name;
        reward.description = description;
        reward.img = filename;
        reward.shop_id = shop_related;
        reward.quantity = quantity;
        reward.points = points;
        try {
            reward.save();
        }
        catch (error){
            throw new Error(error)
        }

        
        return reward;
    }

    async updateReward(updaterewardDto: UpdateRewardDto): Promise<Reward> {
        const {id, name, quantity, description,points} = updaterewardDto;
        const reward = await this.findOne(id);

        if(name){
          reward.name = name;
        }
        if(description){
          reward.description = description;
        }
        if(quantity){
            reward.quantity = quantity;
        }
        if(points){
          reward.points = points
        }
        await reward.save();
        return reward;
    }

    async buyReward(reward:Reward): Promise<Reward> {
        reward.quantity = reward.quantity - 1;
        try {
            reward.save();
        }
        catch (error){
            throw new Error(error)
        }

        return reward;
    }
}
