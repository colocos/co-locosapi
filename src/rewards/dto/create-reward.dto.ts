import { IsNotEmpty, IsNumber, isString, IsString } from "class-validator";

export class CreateRewardDto {
    @IsNotEmpty()
    @IsString()
    name: string;

    description: string;
    shop_id: number;
    quantity: number;
    points: number;

}