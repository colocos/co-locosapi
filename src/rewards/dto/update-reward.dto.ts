import { Shop } from "src/shops/shop.entity";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { OneToMany } from "typeorm";

export class UpdateRewardDto {
    @IsNotEmpty()
    id: number;
    @IsNotEmpty()
    @IsString()
    name: string;
    @IsString()
    description: string;
    @IsNumber()
    quantity: number;
    @IsNumber()
    points: number;

}