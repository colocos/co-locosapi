import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UploadedFile, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { Shop } from 'src/shops/shop.entity';
import { Roles } from 'src/user/auth/roles.decorator';
import { RoleEnum } from 'src/user/auth/roles.enum';
import { RolesGuard } from 'src/user/auth/roles.guard';
import { User } from 'src/user/user.entity';
import { CreateRewardDto } from './dto/create-reward.dto';
import { UpdateRewardDto } from './dto/update-reward.dto';
import { Reward } from './reward.entity';
import { RewardsService } from './rewards.service';
import { GetUser } from 'src/user/get-user.decorator';


@Controller('rewards')
export class RewardsController {
    constructor(private rewardsService: RewardsService){}

    @Get()
    async getRewards(): Promise<Reward[]>{
        return this.rewardsService.getRewards();
    }

    @Get("/:id")
    async getRewardById(@Param("id", ParseIntPipe)id: number): Promise<Reward>{
        return this.rewardsService.getRewardById(id);
    }
    
    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard("jwt"), RolesGuard)
    @Roles(RoleEnum.Admin)
    @UseInterceptors(FileInterceptor('file', {
      storage: diskStorage({
        destination: 'upload/shop',
        filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        return cb(null, `${randomName}${extname(file.originalname)}`)
        }
      })
    }))
    @Post()
    async createReward(@Body() createRewardDto: CreateRewardDto,@UploadedFile() file: Express.Multer.File): Promise<Reward>{
        return this.rewardsService.createReward(createRewardDto,file.filename);
    }    

    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard("jwt"), RolesGuard)
    @Roles(RoleEnum.Admin)
    @Patch()
    async updateReward(@Body() updateRewardDto: UpdateRewardDto): Promise<Reward>{
        return this.rewardsService.updateReward(updateRewardDto);
    }

    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard("jwt"), RolesGuard)
    @Roles(RoleEnum.Admin)
    @Delete(":id")
    async deleteReward(@Param("id", ParseIntPipe) id: number){
        this.rewardsService.deleteReward(id);
    }

    @UsePipes(ValidationPipe)
    @UseGuards(AuthGuard("jwt"))
    @Post(':id/buyReward')
    async buyReward(@GetUser() user: User,@Param('id') id: string): Promise<Reward>{
        return this.rewardsService.buyReward(user,id);
    }

    


}
