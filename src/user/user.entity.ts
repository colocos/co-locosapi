import { Company } from "src/company/company.entity";
import { MyReward } from "src/myreward/entities/myreward.entity";
import { Request } from "src/request/request.entity";
import { Reward } from "src/rewards/reward.entity";
import { Team } from "src/team/team.entity";
import { BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { RoleEnum } from "./auth/roles.enum";

@Entity()
@Unique(["mail"])
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    firstname : String;

    @Column()
    lastname: String;


    @Column()
    city: String;

    @Column()
    bio: String;

    @Column({ nullable: true })
    status: Boolean;

    @Column({ nullable: true })
    img: String;

    @Column({ nullable: true })
    job: String;

    @ManyToOne(() => Team, team => team.users)
    @JoinColumn()
    team: Team;
    
    @ManyToOne(() => Company, company => company.users)
    @JoinColumn()
    public company: Company;

    @Column()
    birthdate: String;

    @Column({ unique: true })
    mail: String;

    @Column({ select: false })
    password: String;

    @OneToMany(() => Request, request => request.user,)
    requests: Request[];

    @OneToMany(() => Request, request => request.author)
    createdRequests: Request[];
    @Column({ default: "user" })
    roles: RoleEnum

    @ManyToMany(()=> Reward)
    @JoinTable()
    rewards: Reward[]


    @Column({ default: 100 })
    points: number;
    
    @OneToMany(() => MyReward, myReward => myReward.user)
    buyedRewards: MyReward[];

}