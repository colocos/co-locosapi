import { Body, Controller, Delete, Get, InternalServerErrorException, NotAcceptableException, Param, ParseIntPipe, Patch, Post, UploadedFile, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { UsersService } from './user.service';
import { User } from './user.entity';
import { createUserDto } from './dto/createUser.dto';
import { connexionDto } from './dto/connexion.dto'; 
import { JwtPayload } from './jwtPayload';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { AuthGuard } from '@nestjs/passport';
import { SimpleConsoleLogger } from 'typeorm';
import { GetUser } from './get-user.decorator';
import { editUserDto } from './dto/editUser.dto';
import { RolesGuard } from './auth/roles.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService,private JwtService: JwtService) {}

  /* @Get()
   */

  // SECURE GET USER DATA
 /*  @Get('/myprofile')
  @UseGuards(AuthGuard("jwt"))
  getMyProfile(@GetUser()user: User): Promise<User>{
    
    //return this.userService.getUserProfile(user.id);
  } */
  //UNSECURE GET USER DATA  
 /*  @Get('/profile/:id')
  @UseGuards(AuthGuard("jwt"))
  getUserProfile(@Param('id',ParseIntPipe)id: number): Promise<User>{
    //return this.userService.getUserProfile(id);
  } */

  //Secure
  @Patch('editUser')
  @UseGuards(AuthGuard("jwt"))
  editUser(@Body() editUserDto: editUserDto ): Promise<User>{
    return this.userService.editUser(editUserDto);
  }

  @Get()
  @UseGuards(AuthGuard("jwt"), RolesGuard)
  getUsers(): Promise<User[]>{
    return this.userService.getUsersProfiles();
  }

  @Get('/myprofile')
  @UseGuards(AuthGuard("jwt"))
  getMyProfile(@GetUser()user: User): Promise<User>{
    return this.userService.getUserProfile(user.id);
  }
  //ALL
  @Post('signin')
  async connexion(@Body() connexionDto: connexionDto) {
    const userCheck = await this.userService.checkConnexion(connexionDto);
    if(userCheck)
    {
      const { id,mail } = userCheck;
      const payload: JwtPayload = {id,mail};
      const accessToken = await this.JwtService.sign(payload);
      const user = await this.userService.getUserProfile(id);
      return { user,accessToken,id };
    }
    else{
      throw new NotAcceptableException("Problem occured in signin.");
    }
  }
  @Post('createUser')
  @UsePipes(ValidationPipe)
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: 'upload/profile',
      filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        return cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  createUser(@Body() createUserDto: createUserDto,@UploadedFile() file: Express.Multer.File): Promise<User>{
    
    return this.userService.createUser(createUserDto,file);
  }

  //NEED TO BY CONTROLLED BY A ROLE
/*   @Delete(':id')
  deleteUserById(@Param() params): Promise<string> {
    
  //return this.userService.deleteUserById(params.id);
  } */


  //CONNEXION / LOGOUT / MIDDLEWARE
}