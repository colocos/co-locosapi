import { EntityRepository, Repository } from "typeorm";
import { createUserDto } from "./dto/createUser.dto";
import { editUserDto } from "./dto/editUser.dto";
import { User } from "./user.entity";
import * as bcrypt from 'bcrypt';
import { Company } from "src/company/company.entity";
import { Team } from "src/team/team.entity";

export const saltOrRounds = 10;

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  
  async createUser(createUserDto: createUserDto,company: Company,filename: string,team: Team): Promise<User>{
    const { firstname,lastname,password,mail,bio,birthdate,city,status,job} = createUserDto;

    const newUser = new User();
    newUser.mail = mail;
    newUser.password = await bcrypt.hash(password, saltOrRounds);
    newUser.firstname = firstname;
    newUser.lastname = lastname;
    newUser.bio = bio;
    newUser.birthdate = birthdate;
    newUser.city = city;
    newUser.company = company;
    newUser.rewards = [];
    newUser.img = filename;
    newUser.job = job;
    newUser.team = team;
    if(status !== undefined)
    {
      newUser.status = status;
    }

    try{
      await newUser.save();
    }
    catch(err) {
      
      throw new Error(err);
    }

    return newUser;

  }

  async editUser(user: editUserDto,company: Company): Promise<User> {
    const { id,bio,city,img,status } = user;

    const editUser = await this.findOne(id);

    if(bio){
      editUser.bio = bio;
    }
    if(city){
      editUser.city = city;
    }
    if(img){
      editUser.img = img;
    }
    if(status != undefined){
      editUser.status = status;
    }
    if(company){
      editUser.company = company;
    }

    try {
      editUser.save();
    }
    catch(err) {
      throw new Error(err);
    }

    return editUser;

  }

  async updateUserPoints(user: User,points: number): Promise<User> {
    user.points = user.points - points;
    try {
      user.save();
    }
    catch(err) {
      throw new Error(err);
    }

    return user;
  }

}
