import { IsNotEmpty, IsString,IsNumber, IsBoolean, IsEnum, IsArray } from "class-validator";
import { RoleEnum } from "../auth/roles.enum";
export class createUserDto {
  
  @IsString()
  @IsNotEmpty()
    password: String;
  @IsNotEmpty()
    mail: String;
  @IsString()
  @IsNotEmpty()
    lastname: String;
  @IsString()
  @IsNotEmpty()
    firstname: String;
  @IsString()
  @IsNotEmpty()
    bio: String;
  @IsString()
  @IsNotEmpty()
    birthdate: String;
  @IsString()
  @IsNotEmpty()
    city: String;
  @IsString()
  @IsNotEmpty()
    job: String;
  /* @IsString()
  @IsNotEmpty()
    img: String; */
  @IsNotEmpty()
    companyId: number;
    status: Boolean;
  teamId: number;
}