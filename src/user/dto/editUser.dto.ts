import { IsNotEmpty, IsString,IsNumber, IsBoolean, IsEnum } from "class-validator";
import { RoleEnum } from "../auth/roles.enum";
export class editUserDto {
  @IsNumber()
    id: number;
  @IsString()
    bio: String;
  @IsString()
    city: String;
  @IsString()
    img: String;
  @IsBoolean()
    status: Boolean;
  @IsString()
    companyId: number;
  @IsEnum(RoleEnum)
    roles: RoleEnum
  @IsNumber()
    points: number
}