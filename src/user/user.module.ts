import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UsersController } from './user.controller';
import { UsersService } from './user.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.stategy';
import { jwtConstants } from './constants';
import { CompanyModule } from '../company/company.module';
import { TeamModule } from 'src/team/team.module';



@Module({
  imports: [
    PassportModule.register(JwtStrategy),
    JwtModule.register({​
    secret: jwtConstants.secret,​
    signOptions: {​
      expiresIn: 3600,​
    },​}),
    TypeOrmModule.forFeature([UserRepository]),CompanyModule,TeamModule],
  controllers: [UsersController],
  providers: [UsersService,JwtStrategy],
  exports: [
    JwtStrategy,
    PassportModule,
    UsersService
  ]
})
export class UserModule {}
