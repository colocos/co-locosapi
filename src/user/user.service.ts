import { Injectable, InternalServerErrorException, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { createUserDto } from './dto/createUser.dto';
import { connexionDto } from './dto/connexion.dto';
//import { errorConnexionDto } from "./errorConnexion.dto";
import * as bcrypt from 'bcrypt';
import { editUserDto } from './dto/editUser.dto';
import { CompanyService } from 'src/company/company.service';
import { Raw } from 'typeorm';
import { TeamService } from 'src/team/team.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
     private UserRepository: 
     UserRepository,
     private companyService:
     CompanyService,
     private teamService:
     TeamService
  ) {}

  async getUsersProfiles(): Promise<User[]>{
    const allUsers = await this.UserRepository.find({select: ["id","mail"]});

    if(!allUsers)
    {
      throw new NotFoundException("NOTHING ON DB");
    }
    return allUsers;
  }

  async getUserProfile(id: number): Promise<User>{

    let found = await this.UserRepository.findOne(id,{relations: ["company","requests","buyedRewards","team"]});

    let newRequests = found.requests.filter(request => request.refreshDate > new Date());
    found.requests = newRequests;
    if(!found)
    {
      throw new NotFoundException(`Task With id = ${id} NOT FOUND`);
    }

    return found;
  }

  async createUser(user: createUserDto,file: Express.Multer.File): Promise<User>{
    const { filename } = file;
    const company = await this.companyService.getCompanyById(user.companyId);
    const team = await this.teamService.getTeamById(user.teamId);
    const newUser = this.UserRepository.createUser(user,company,filename,team);
    return newUser;
  }

  async editUser(user: editUserDto): Promise<User>{
    const company = await this.companyService.getCompanyById(user.companyId);
    return this.UserRepository.editUser(user,company);
  }

  /* async deleteUserById(id): Promise<string>{
    if(this.getUserProfile(id))
    {
      await this.UserRepository.delete(id);

      return "User deleted.";
    }
    else{
      return "No users were found.";
    }
  } */

  async checkConnexion(connexionDto: connexionDto): Promise<User>{
    const { mail,password } = connexionDto;

    const foundedUser = await this.UserRepository.findOne({select: ["password","id"],where: {mail}});

    if(foundedUser){
      const checkedPass = await bcrypt.compare(password, foundedUser.password)
      if(checkedPass)
      {
        return await this.getUserProfile(foundedUser.id);
      }
      else{
        throw new InternalServerErrorException("Invalid Pass/Mail.")
      }
    }
    else{
      throw new NotFoundException("No user founded.");
    }
  }

  async updateUserPoints(user: User,points: number): Promise<User>{
    if(user.points - points > 0)
    {
      return this.UserRepository.updateUserPoints(user,points);
    }
    else{
      throw new InternalServerErrorException("Too few points");
    }
  }
  
}
