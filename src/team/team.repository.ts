import { Company } from "src/company/company.entity";
import { User } from "src/user/user.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateTeamDto } from "./dto/create-team.dto";
import { UpdateTeamDto } from "./dto/update-team.dto";
import { Team } from "./team.entity";

@EntityRepository(Team)
export class TeamRepository extends Repository<Team> {

  async createTeam(createTeamDto: CreateTeamDto,company: Company): Promise<Team>{
    const { name } = createTeamDto;

    const newTeam = new Team();
    newTeam.name = name;
    newTeam.company = company;
    try{
      await newTeam.save();
    }
    catch(err) {
      throw new Error(err);
    }

    return newTeam;

  }

  async editTeam(updateTeamDto: UpdateTeamDto,users: User[]): Promise<Team> {
    const { id,name } = updateTeamDto;

    const editTeam = await this.findOne(id);

    if(name){
      editTeam.name = name;
    }
    if(users){
      editTeam.users = users;
    }

    try {
      editTeam.save();
    }
    catch(err) {
      throw new Error(err);
    }

    return editTeam;

  }
}