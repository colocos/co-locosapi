import { Company } from "src/company/company.entity";
import { User } from "src/user/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Team extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => User,user => user.team)
  users: User[];

  @ManyToOne(() => Company,company => company.teams)
  @JoinColumn()
  company: Company;
}
