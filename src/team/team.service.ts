import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyService } from 'src/company/company.service';
import { UserRepository } from 'src/user/user.repository';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { TeamRepository } from './team.repository';
import { UsersService } from 'src/user/user.service';
import { User } from 'src/user/user.entity';
import { Team } from './team.entity';

@Injectable()
export class TeamService {
  constructor(
    @InjectRepository(TeamRepository)
     private teamRepository: 
     TeamRepository,
     private companyService:
     CompanyService
  ){}


  async create(createTeamDto: CreateTeamDto): Promise<Team> {
    let { users,company } = createTeamDto;

    let newCompany = await this.companyService.getCompanyById(company);
    return this.teamRepository.createTeam(createTeamDto, newCompany);
  }

  async getTeamById(id: number): Promise<Team> {
    return this.teamRepository.findOne(id);
  }

  findAll() {
    return `This action returns all team`;
  }

  findOne(id: number) {
    return `This action returns a #${id} team`;
  }

  update(id: number, updateTeamDto: UpdateTeamDto) {
    return `This action updates a #${id} team`;
  }

  remove(id: number) {
    return `This action removes a #${id} team`;
  }
}
