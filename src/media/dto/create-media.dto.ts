import { IsNotEmpty } from "class-validator";
import { Request } from "src/request/request.entity";

export class CreateMediaDto {
  @IsNotEmpty()
  filename: string;
  @IsNotEmpty()
  name: string;
  
  request: Request;
}
