import { Request } from "src/request/request.entity";
import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Media extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  filename: string;

  @Column()
  name: string;

  @ManyToOne(() => Request,request => request.medias)
  request: Request;
}
