import { EntityRepository, Repository } from "typeorm";
import { Media } from "./media.entity";
import { Request } from "../request/request.entity";
import { CreateMediaDto } from "./dto/create-media.dto";

@EntityRepository(Media)
export class MediaRepository extends Repository<Media> {

  async createMedia(createMediaDto: CreateMediaDto): Promise<Media> {
    const { filename,name,request } = createMediaDto;
    let newMedia = new Media();
    newMedia.filename = filename;
    newMedia.name = name;
    if(request) {
      newMedia.request = request;
    }
    newMedia.save();
    return null;
  }
}