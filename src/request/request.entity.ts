import { Company } from "src/company/company.entity";
import { Media } from "src/media/media.entity";
import { User } from "src/user/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, Unique } from "typeorm";

@Entity()
export class Request extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  name: String;

  @Column()
  description: String;

  @Column()
  point: number; 

  @Column({ nullable: true })
  status: number;

  @ManyToOne(() => User,user => user.requests,{eager: true})
  @JoinColumn()
  user: User;

  @Column({ nullable: true })
  refreshDate: Date;

  @Column({ nullable: true })
  finishDate: Date;

  @OneToMany(() => Media,media => media.request, {eager: true})
  medias: Media[];

  @ManyToOne(() => Company,company => company.requests)
  @JoinColumn()
  company: Company;
  
  @ManyToOne(() => User,user => user.createdRequests,{eager: true})
  @JoinColumn()
  author: User;

  @Column({ default: false })
  isFinish: boolean;

  @Column({ default: false })
  isInValidation: boolean;

  @Column({ default: false })
  isCancel: boolean;
}