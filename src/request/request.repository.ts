import { Company } from "src/company/company.entity";
import { User } from "src/user/user.entity";
import { EntityRepository, Repository } from "typeorm";
import { createRequestDto } from "./dto/create-request.dto";
import { updateRequestDto } from "./dto/update-request.dto";
import { Request } from "./request.entity";

@EntityRepository(Request)
export class RequestRepository extends Repository<Request> {
  
  async createRequest(createRequestDto: createRequestDto,company: Company,user: User): Promise<Request>{
    const { name,description,point } = createRequestDto;
    const newRequest = new Request();

    newRequest.name = name;
    newRequest.description = description;
    newRequest.point = point;
    newRequest.author = user;
    newRequest.company = company;

    try{
      await newRequest.save();
    }
    catch(err){
      throw new Error(err);
    }
    return newRequest;
  }

  async editRequest(id: number,updateRequestDto: updateRequestDto): Promise<Request>{
    const { name,description,point,status,refreshDate } = updateRequestDto;
    let editedRequest = await this.findOne(id);

    if(name != null){
      editedRequest.name = name;
    }
    if(description != null){
      editedRequest.description = description;
    }
    if(point != null){
      editedRequest.point = point;
    }

    /* if(status != null){
      editedRequest.status = status;
    } */

    if(refreshDate != null)
    {
      editedRequest.refreshDate = refreshDate;
    }

    try{
      await editedRequest.save();
    }
    catch(err){
      throw new Error(err);
    }
    return editedRequest;
  }
  async associateRequestToUser(user: User,requestId: number): Promise<Request> {
    let editedRequest = await this.findOne(requestId);
    let actualDate = new Date();
    let newDate = new Date(actualDate.getTime() + 60*60000);
    editedRequest.user = user;
    editedRequest.refreshDate = newDate;
    editedRequest.status = 1;

    try{
      await editedRequest.save();
    }
    catch(err){
      throw new Error(err);
    }
    return editedRequest;

  }

  async resolveRequest(request: Request): Promise<Request> {
    request.refreshDate = null;
    request.status = 2;
    request.isInValidation = true;
    try{
      await request.save();
    }
    catch(err){
      throw new Error(err);
    }
    return request;
  }

  async finishRequest(request: Request): Promise<Request> {
    request.refreshDate = null;
    request.isFinish = true;
    request.isCancel = false;
    request.isInValidation = false;
    request.status = 3;
    request.finishDate = new Date();
    try{
      await request.save();
    }
    catch(err){
      throw new Error(err);
    }
    return request;
  }

  async cancelRequest(request: Request): Promise<Request> {
    request.refreshDate = null;
    request.status = null;
    request.isCancel = false;
    request.isInValidation = false;
    try{
      await request.save();
    }
    catch(err){
      throw new Error(err);
    }
    return request;
  }

  async giveUpRequest(request: Request): Promise<Request> {
    request.refreshDate = null;
    request.status = null;
    request.user = null;
    try{
      await request.save();
    }
    catch(err){
      throw new Error(err);
    }
    return request;
  }
}
