import { Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMediaDto } from 'src/media/dto/create-media.dto';
import { Media } from 'src/media/media.entity';
import { MediaRepository } from 'src/media/media.repository';
import { MediaService } from 'src/media/media.service';
import { CompanyService } from 'src/company/company.service';
import { User } from 'src/user/user.entity';
import { UsersService } from 'src/user/user.service';
import { Equal, Raw } from 'typeorm';
import { createRequestDto } from './dto/create-request.dto';
import { updateRequestDto } from './dto/update-request.dto';
import { Request } from './request.entity';
import { RequestRepository } from './request.repository';
import { Company } from 'src/company/company.entity';

@Injectable()
export class RequestService {
  
  constructor(
    @InjectRepository(RequestRepository)
     private RequestRepository: 
     RequestRepository,
     private userService: 
     UsersService,
     private mediaService: 
     MediaService,
     private companyService: 
     CompanyService
  ) {}

  async create(request: createRequestDto,user: User): Promise<Request> {
    const { companyId } = request;
    const company = await this.companyService.getCompanyById(companyId);
    const newRequest = this.RequestRepository.createRequest(request,company,user);
    return newRequest;
  }

  async findAll(): Promise<Request[]> {
    const allRequest = await this.RequestRepository.find({refreshDate: Raw((d) => `${d} < NOW() OR ${d} IS NULL`)});

    if(!allRequest)
    {
      throw new NotFoundException("NOTHING ON DB");
    }
    return allRequest;
  }

  async findByCompanyId(companyId: number): Promise<Request[]>{
    const requestsAvailableByCompany = await this.RequestRepository.find({where: {refreshDate: Raw((d) => `${d} < NOW() OR ${d} IS NULL`),isFinish: false,company: companyId,status: null}});
    return requestsAvailableByCompany;
  }

  async findAllByCompanyId(company: number): Promise<Request[]>{
    const requestsAvailableByCompany = await this.RequestRepository.find({where: {company: company},relations: ["company","author","user"]});
    return requestsAvailableByCompany;
  }
  async update(id: number, updateRequestDto: updateRequestDto): Promise<Request> {
    const editedRequest = await this.RequestRepository.editRequest(id, updateRequestDto);
     
    return editedRequest;
  }

  async findRequestByUserId(id: number): Promise<Request[]> {
    const requests = await this.RequestRepository.find({where: {refreshDate: Raw((d) => `${d} > NOW() OR ${d} IS NULL`),user: {id: id}}});
    let newRequests = requests.filter(request => request.refreshDate === null ? request.status != null : true);
    return newRequests;
  }

  async associateRequestToUser(user: User,requestId: number): Promise<Request> {
    const editedRequest = this.RequestRepository.associateRequestToUser(user,requestId);
    return editedRequest;
  }

  remove(id: number) {
    try{
      return this.RequestRepository.delete(id) ? true : false;
    }
    catch(err){
      return false;
    }
  }

  async resolveRequest(requestId: number,requestUser: User,name: string,filename: string): Promise<Request> {
    const editedRequest = await this.RequestRepository.findOne(requestId,{relations: ["user"]});
    const { id,user } = editedRequest;
    let mediaDto = new CreateMediaDto();
    mediaDto.filename = filename;
    mediaDto.name = name;
    mediaDto.request = editedRequest;
    if(user.id === requestUser.id) {
      this.mediaService.create(mediaDto);
      user.points = user.points + editedRequest.point;
      await user.save();
      const requestValidate = this.RequestRepository.resolveRequest(editedRequest);
      return requestValidate;
    }
    else {
      throw new NotAcceptableException("User did not fit with request user");
    }
  }

  async finishRequest(requestId: string): Promise<Request> {
    const editedRequest = await this.RequestRepository.findOne(requestId,{relations: ["user"]});
    return this.RequestRepository.finishRequest(editedRequest);
  }

  async cancelRequest(requestId: string): Promise<Request> {
    const editedRequest = await this.RequestRepository.findOne(requestId,{relations: ["user"]});
    return this.RequestRepository.cancelRequest(editedRequest);
  }

  async giveUpRequest(user: User,requestId: string): Promise<Request> {
    const editedRequest = await this.RequestRepository.findOne(requestId,{relations: ["user"]});
    if(user.id == editedRequest.user.id) {
      return this.RequestRepository.giveUpRequest(editedRequest);
    }
    else {
      throw new NotAcceptableException("User did not fit with request user");
    }
  }
}
