import { Module } from '@nestjs/common';
import { RequestService } from './request.service';
import { RequestController } from './request.controller';
import { RequestRepository } from './request.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from 'src/user/user.module';
import { MediaModule } from 'src/media/media.module';
import { CompanyModule } from 'src/company/company.module';

@Module({
  imports:[MediaModule,UserModule,TypeOrmModule.forFeature([RequestRepository]),CompanyModule],
  controllers: [RequestController],
  providers: [RequestService]
})
export class RequestModule {}
