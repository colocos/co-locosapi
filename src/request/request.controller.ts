import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, UseInterceptors, UploadedFile, NotAcceptableException } from '@nestjs/common';
import { RequestService } from './request.service';
import { createRequestDto } from './dto/create-request.dto';
import { updateRequestDto } from './dto/update-request.dto';
import { AuthGuard } from '@nestjs/passport';
import { Request } from './request.entity';
import { GetUser } from 'src/user/get-user.decorator';
import { User } from 'src/user/user.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { RolesGuard } from 'src/user/auth/roles.guard';
import { RoleEnum } from 'src/user/auth/roles.enum';
import { Roles } from 'src/user/auth/roles.decorator';

@Controller('request')
export class RequestController {
  constructor(private readonly requestService: RequestService) {}

  @Post()
  @UseGuards(AuthGuard("jwt"))
  create(@GetUser()user: User,@Body() createRequestDto: createRequestDto) {
    return this.requestService.create(createRequestDto,user);
  }
  @Get("admin/:id")
  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Roles(RoleEnum.Admin)
  findAllRequestByCompanyId(@GetUser()user: User,@Param('id') companyId: string): Promise<Request[]> {
    return this.requestService.findAllByCompanyId(parseInt(companyId));
  }

  @Get(':id')
  @UseGuards(AuthGuard("jwt"))
  findRequestByCompanyId(@Param('id') companyId: string) {
    return this.requestService.findByCompanyId(parseInt(companyId));
  }

  @Get('myrequests')
  @UseGuards(AuthGuard("jwt"))
  getMyRequests(@GetUser()user: User): Promise<Request[]> {
    const { id } = user;
    return this.requestService.findRequestByUserId(id);
  }

  @Get('user/:id')
  @UseGuards(AuthGuard("jwt"))
  async getUserRequests(@Param('id') id: number): Promise<Request[]> {
    return this.requestService.findRequestByUserId(id);
  }

  getRequestByStatus(){

  }

  @Patch(':id')
  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Roles(RoleEnum.Admin)
  async update(@Param('id') id: number, @Body() updateRequestDto: updateRequestDto): Promise<Request> {
    return this.requestService.update(+id, updateRequestDto);
  }

  @Patch('associate/:id')
  @UseGuards(AuthGuard("jwt"))
  async associateRequestToUser(@GetUser()user: User,@Param("id") id: number): Promise<Request> {
    return this.requestService.associateRequestToUser(user,id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Roles(RoleEnum.Admin)
  remove(@Param('id') id: string) {
    return this.requestService.remove(+id) ? "Request deleted" : "Request not found";
  }

  @Post(':id/resolve')
  @UseGuards(AuthGuard("jwt"))
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: 'upload/requestResolveImage',
      filename: (req, file, cb) => {
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
        return cb(null, `${randomName}${extname(file.originalname)}`)
      }
    })
  }))
  async resolveRequest(@GetUser() user: User,@Param('id') id: string,@UploadedFile() file: Express.Multer.File) {
    if(file) {
      return this.requestService.resolveRequest(parseInt(id),user,file.originalname,file.filename);
    }
    else{
      throw new NotAcceptableException("No file added on request");
    }
  }

  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Roles(RoleEnum.Admin)
  @Patch('/finish/:id')
  async finishRequest(@GetUser() user: User,@Param('id') id: string): Promise<Request>{
    return this.requestService.finishRequest(id);
  }

  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Roles(RoleEnum.Admin)
  @Patch('/cancel/:id')
  async cancelRequest(@GetUser() user: User,@Param('id') id: string): Promise<Request>{
    return this.requestService.cancelRequest(id);
  }

  @Patch('/giveup/:id')
  @UseGuards(AuthGuard("jwt"))
  async giveUpRequest(@GetUser() user: User,@Param('id') id: string): Promise<Request>{
    return this.requestService.giveUpRequest(user,id);
  }
}
