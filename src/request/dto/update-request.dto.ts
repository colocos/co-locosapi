import { PartialType } from '@nestjs/mapped-types';
import { createRequestDto } from './create-request.dto';

export class updateRequestDto extends PartialType(createRequestDto) {

  status: string;
  userId: number;
  refreshDate: Date;
}
