import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class createRequestDto {
  @IsString()
  @IsNotEmpty()
    name: String;
  @IsString()
  @IsNotEmpty()
    description: String;
  @IsNumber()
  @IsNotEmpty()
    point: number;
  @IsNumber()
  @IsNotEmpty()
    companyId: number;
}
